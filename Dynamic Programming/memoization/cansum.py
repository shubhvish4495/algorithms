

from operator import truediv


def canSum(tarSum,numbers):
    if tarSum == 0:
        return True
    if tarSum < 0:
        return False
    for m in numbers:
        rem =  tarSum - m
        if canSum(rem,numbers):
            return True
    
    return False

print(canSum(7,[2,3]),canSum(7,[2,4]))