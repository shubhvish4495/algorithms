Memoization recipe:

1. Make it work (try recursive)
    - Visulaize as tree
    - Implement tree using recursion
    - test it

2. Make it efficient
    - add a memo object
    - add abase case to return memo values
    - store returned values into the memo