def union(sz,arr,p,q):
    pI =  findRoot(arr,p)
    qI = findRoot(arr,q)
    if pI == qI:
        return
    if sz[pI] > sz[qI]:
        arr[qI] = pI
        sz[pI] += sz[qI]
    else:
        arr[pI] = qI
        sz[qI] += sz[pI]
    print(arr,sz)


def connected(arr,p,q):
    return findRoot(arr,p) == findRoot(arr,q)

def findRoot(arr,ele):
    if arr[ele] == ele:
        return arr[ele]
    return findRoot(arr,arr[ele])

n = int(input())
arr = [x  for x in range(n+1)]
sz = [1]*(n+1)
print(arr,sz)
no_of_inputs = int(input())
for m in range(no_of_inputs):
    funcChoice,p,q = map(int,input().split())
    if funcChoice == 1:
        print(connected(arr,p,q))
    else:
        union(sz,arr,p,q)



